import { Component } from '@angular/core';
import * as jsPDF from 'jspdf';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  base64Img = null;
  margins = {
    top: 70,
    bottom: 40,
    left: 30,
    width: 550
  };
generate()
{
  console.log("qwerty")
  var pdf = new jsPDF('p', 'pt', 'a4');
  pdf.setFontSize(18);
  pdf.fromHTML(document.getElementById('html-2-pdfwrapper'), 
    this.margins.left, // x coord
    this.margins.top,
    {
      // y coord
      width: this.margins.width// max width of content on PDF
    },function(dispose) {
      // this.headerFooterFormatting(pdf)
    }, 
    this.margins);
    
  var iframe = document.createElement('iframe');
  iframe.setAttribute('style','position:absolute;right:0; top:0; bottom:0; height:100%; width:650px; padding:20px;');
  document.body.appendChild(iframe);
  
  iframe.src = pdf.output('datauristring');
};
// headerFooterFormatting(doc)
// {
//     var totalPages  = doc.internal.getNumberOfPages();

//     for(var i = totalPages; i >= 1; i--)
//     { //make this page, the current page we are currently working on.
//         doc.setPage(i);      
                      
//         this.header(doc);
        
//         // this.footer(doc, i, totalPages);
        
//     }
// };
// header(doc)
// {
//     doc.setFontSize(30);
//     doc.setTextColor(40);
//     doc.setFontStyle('normal');
  
//     if (this.base64Img) {
//        doc.addImage(this.base64Img, 'JPEG', this.margins.left, 10, 40,40);        
//     }
      
//     doc.text("Report Header Template", this.margins.left + 50, 40 );
  
//     doc.line(3, 70, this.margins.width + 43,70); // horizontal line
// };
// imgToBase64(url, callback, imgVariable) {
 
//   if (!window) {
//       callback(null);
//       return;
//   }
//   var xhr = new XMLHttpRequest();
//   xhr.responseType = 'blob';
//   xhr.onload = function() {
//       var reader = new FileReader();
//       reader.onloadend = function() {
//     imgVariable = reader.result.replace('text/xml', 'image/jpeg');
//           callback(imgVariable);
//       };
//       reader.readAsDataURL(xhr.response);
//   };
//   xhr.open('GET', url);
//   xhr.send();
// };

}

